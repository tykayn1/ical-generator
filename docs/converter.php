<?php
/**
 ** @author : B. Lemoine / CipherBliss / contact@ciperhbliss.com
 * based on this stackoverflow question
 * https://stackoverflow.com/questions/1463480/how-can-i-use-php-to-dynamically-publish-an-ical-file-to-be-read-by-google-calen#1464355
 *
 * - validate your calendar here: https://icalendar.org/validator.html
 */
/**
 ** import must use the example format from example.csv
 * you can tweak this to use an other format
 **/

function importCSV() {
// TODO
}

function addEvent( $dateStart, $dateEnd, $title, $url, $description, $summary ) {

	$events = '';
	// TODO
	$newEvent = "BEGIN:VEVENT" . $eol .
	            "DTEND:" . dateToCal( $end ) . $eol .
	            "UID:" . $id . $eol .
	            "DTSTAMP:" . dateToCal( time() ) . $eol .
	            "DESCRIPTION:" . htmlspecialchars( $title ) . $eol .
	            "URL;VALUE=URI:" . htmlspecialchars( $url ) . $eol .
	            "SUMMARY:" . htmlspecialchars( $description ) . $eol .
	            "DTSTART:" . dateToCal( $start ) . $eol .
	            "END:VEVENT" . $eol;

	return $events;
}

function dateToCal( $timestamp ) {
	return date( 'Ymd\Tgis\Z', $timestamp );
}

function escapeString( $string ) {
	return preg_replace( '/([\,;])/', '\\\$1', $string );
}

$events = '';
$eol    = "\r\n";
$ical   = "BEGIN:VCALENDAR" . $eol .
          "VERSION:2.0" . $eol .
          "PRODID:-//project/author//NONSGML v1.0//EN" . $eol .
          "CALSCALE:GREGORIAN" . $eol .
          $events .
          "END:VCALENDAR";


//set correct content-type-header
header( 'Content-type: text/calendar; charset=utf-8' );
header( 'Content-Disposition: inline; filename=calendar.ics' );
echo $ical;
exit;
