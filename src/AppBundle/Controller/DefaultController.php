<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {
	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction( Request $request ) {
		$m        = $this->getDoctrine()->getManager();
		$userRepo = $m->getRepository( 'AppBundle:User' );
		$allUsers = $userRepo->findAll();

		// replace this example code with whatever you need
		return $this->render( 'default/index.html.twig',
			[
				'usersCount' => count( $allUsers ),
				'base_dir'   => realpath( $this->getParameter( 'kernel.project_dir' ) ) . DIRECTORY_SEPARATOR,
			] );
	}

	/**
	 * @Route("/dashboard", name="dashboard")
	 */
	public function dashboardAction( Request $request ) {
		$m        = $this->getDoctrine()->getManager();
		$userRepo = $m->getRepository( 'AppBundle:User' );
		$allUsers = $userRepo->findAll();

		// replace this example code with whatever you need
		return $this->render( 'logged/dashboard.html.twig',
			[
				'usersCount' => count( $allUsers ),
				'base_dir'   => realpath( $this->getParameter( 'kernel.project_dir' ) ) . DIRECTORY_SEPARATOR,
			] );
	}

	/**
	 * @Route("/view-sms", name="view-sms")
	 */
	public function viewSMSAction( Request $request ) {
		$m        = $this->getDoctrine()->getManager();
		$userRepo = $m->getRepository( 'AppBundle:User' );

		// replace this example code with whatever you need
		return $this->render( 'logged/list/sms.html.twig',
			[] );
	}


	/**
	 * @Route("/list_sms", name="list_sms")
	 */
	public function listSmssAction( Request $request, $order = 'ASC' ) {
		$m          = $this->getDoctrine()->getManager();
		$repository = $m->getRepository( 'AppBundle:Sms' );

		$contacts_repository   = $m->getRepository( 'AppBundle:Contact' );
		$currentUser           = $this->getUser();
		$smsFound              = $repository->findBy( [ 'owner' => $currentUser->getId() ],
			[ 'dateReception' => $order ] );
		$found_counter         = 0;
		$corresponding_counter = 0;
		$tableByYears          = [];

		// try to convert dates from date raw DD/MM/YY
		foreach ( $smsFound as $s ) {
			if ( ! isset( $tableByYears[ $s->getDateReception()->format( 'Y' ) ] ) ) {
				$tableByYears[ $s->getDateReception()->format( 'Y' ) ] = [
					'01' => 0,
					'02' => 0,
					'03' => 0,
					'04' => 0,
					'05' => 0,
					'06' => 0,
					'07' => 0,
					'08' => 0,
					'09' => 0,
					'10' => 0,
					'11' => 0,
					'12' => 0,
				];
			}
			$tableByYears[ $s->getDateReception()->format( 'Y' ) ][ $s->getDateReception()->format( 'm' ) ] ++;

//			if ( ! $s->getContent() ) {
//				$m->remove( $s );
//			}
//			if ( '' !== $s->getDateRaw() ) {
//
//				$datetime = new \DateTime();
//				$newDate  = $datetime->createFromFormat( 'd/m/y H:i:s', $s->getDateRaw() . '12:00:00' );
//				$s->setDateReception( $newDate );
//				$m->persist( $s );
//			} else {
//				var_dump( $s->getDateRaw() );
//				echo "pas de date pour le message : " . $s->getContent();
//			}
			// trouver une correspondance de contact
			$telToSearch = trim( $s->getSenderTel() );
			if ( $s->getSender() ) {
				$corresponding_counter ++;
			} else {
				$found = $contacts_repository->findOneByTelMobile( $telToSearch );
				if ( ! $found ) {
					if ( preg_match( '/^33/', $telToSearch ) ) {
						// the number begins with 33, instead of +33
						$found = $contacts_repository->findOneByTelMobile( '+' . $telToSearch );
						// the number begins with 33, remove the 33
						if ( ! $found ) {
							$found = $contacts_repository->findOneByTelMobile( substr( $telToSearch, 2 ) );
						}
					} elseif ( preg_match( '/^[67]/', $telToSearch ) ) {
						// the number begins qui 6 or 7 instead of 06 or 07

						if ( ! $found ) {
							$found = $contacts_repository->findOneByTelMobile( '0' . $telToSearch );
						}
					}
				}


				if ( ! $found ) {
					$found = $contacts_repository->findOneByTel( $s->getSenderTel() );
				}

				if ( $found ) {
					$found_counter ++;
					$found->addSmsSent( $s );
					$s->setSender( $found );
					$m->persist( $s );
					$m->persist( $found );
				}
			}
		}

		$m->flush();


		return $this->render( ':logged/list:sms.html.twig',
			[
				'tableByYears'          => $tableByYears,
				'sms'                   => $smsFound,
				'found_counter'         => $found_counter,
				'corresponding_counter' => $corresponding_counter,
			] );
	}

	/**
	 * @Route("/list_conversations", name="list_conversations")
	 */
	public function listConversationsAction( Request $request, $order = 'ASC' ) {
		$m             = $this->getDoctrine()->getManager();
		$userRepo      = $m->getRepository( 'AppBundle:Conversation' );
		$currentUser   = $this->getUser();
		$conversations = $userRepo->findBy( [ 'owner' => $currentUser->getId() ], [ 'id' => $order ] );

		$m->flush();


		return $this->render( 'logged/stats/conversations.html.twig',
			[ 'conversations' => $conversations ] );
	}

	/**
	 * @Route("/list_contacts", name="list_contacts")
	 */
	public function listContactsAction( Request $request, $order = 'ASC' ) {
		$m           = $this->getDoctrine()->getManager();
		$userRepo    = $m->getRepository( 'AppBundle:Contact' );
		$currentUser = $this->getUser();
		$contact     = $userRepo->findBy( [ 'owner' => $currentUser->getId() ], [ 'firstName' => $order ] );

		$m->flush();


		return $this->render( ':logged/list:contacts.html.twig',
			[ 'contacts_list' => $contact ] );
	}

	/**
	 * @Route("/list_evenementss", name="list_evenementss")
	 */
	public function listEvenementssAction( Request $request, $order = 'ASC' ) {
		$m           = $this->getDoctrine()->getManager();
		$userRepo    = $m->getRepository( 'AppBundle:Evenements' );
		$currentUser = $this->getUser();
		$evenements  = $userRepo->findBy( [ 'owner' => $currentUser->getId() ], [ 'firstName' => $order ] );

		$m->flush();


		return $this->render( ':logged/list:evenements.html.twig',
			[ 'evenements_list' => $evenements ] );
	}

	/**
	 * @Route("/list_search", name="list_search")
	 */
	public function listResearchAction( Request $request, $order = 'ASC' ) {
		$m           = $this->getDoctrine()->getManager();
		$userRepo    = $m->getRepository( 'AppBundle:Research' );
		$currentUser = $this->getUser();
		$research    = $userRepo->findBy( [ 'owner' => $currentUser->getId() ], [ 'date' => $order ] );

		$m->flush();


		return $this->render( ':logged/list:researches.html.twig',
			[ 'research_list' => $research ] );
	}

	/**
	 * @Route("/list_sms_desc", name="list_sms_desc")
	 */
	public function listSmssDescAction( Request $request, $order = 'DESC' ) {

		return $this->listSmssAction( $request, $order );
	}

	/**
	 * @Route("/view-contact/{contactId}", name="view_contact")
	 * @ParamConverter("contactId", class="AppBundle:Contact")
	 */
	public function viewOneContactAction( Contact $contactId ) {

		return $this->render( 'logged/view/contact.html.twig',
			[ 'contact' => $contactId ] );
	}


	/**
	 * @Route("/list_sms_desc", name="list_sms_desc")
	 */
	public function exportAllEventsToCSVAction() {

		$user = $this->getUser();
		if ( ! $user ) {
			return $this->render( 'logged/dashboard.html.twig',
				[ 'counter_importations_done' => 'pas d utilisateur connecté' ] );
		}
// get events of logged in user
		$events = $user->getEvenements();
		// convert to csv
		// return the csv

		return $this->render( 'logged/dashboard.html.twig',
			[ 'counter_importations_done' => 'exportation de x evenements faite' ] );
	}


	/**
	 * @param string $filename
	 * @param string $delimiter
	 *
	 * @return array|bool
	 */
	public function csv_to_array( $filename = '', $delimiter = ',' ) {
		if ( ! file_exists( $filename ) || ! is_readable( $filename ) ) {
			return false;
		}

		$header = null;
		$data   = [];
		if ( ( $handle = fopen( $filename, 'r' ) ) !== false ) {
			while ( ( $row = fgetcsv( $handle, 1000, $delimiter ) ) !== false ) {
				if ( ! $header ) {
					$header = $row;
				} else {
					$data[] = array_combine( $header, $row );
				}
			}
			fclose( $handle );
		}

		return $data;
	}

}
