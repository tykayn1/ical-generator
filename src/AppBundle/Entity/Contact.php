<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 */
class Contact {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * First Name
	 * field n° 0 in google export
	 * @var string
	 *
	 * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
	 */
	private $firstName;

	/**
	 * Middle Name
	 * field n° 1 in google export
	 * @var string
	 *
	 * @ORM\Column(name="middleName", type="string", length=255, nullable=true)
	 */
	private $middleName;
	/**
	 * Last Name
	 * field n° 2 in google export
	 * @var string
	 *
	 * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
	 */
	private $lastName;
	/**
	 * Birthday
	 * field n° 8 in google export
	 * @var string
	 *
	 * @ORM\Column(name="birth_day", type="string", length=255, nullable=true)
	 */
	private $birthDay;
	/**
	 * Notes
	 * field n° 13 in google export
	 * @var string
	 *
	 * @ORM\Column(name="notes", type="string", length=500, nullable=true)
	 */
	private $notes;
	/**
	 * E-mail Address
	 * field n° 14 in google export
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255, nullable=true)
	 */
	private $email;
	/**
	 * E-mail 2 Address
	 * field n° 15 in google export
	 * @var string
	 *
	 * @ORM\Column(name="email2", type="string", length=255, nullable=true)
	 */
	private $email2;
	/**
	 * E-mail 3 Address
	 * field n° 16 in google export
	 * @var string
	 *
	 * @ORM\Column(name="email3", type="string", length=255, nullable=true)
	 */
	private $email3;
	/**
	 * Primary Phone
	 * field n° 17 in google export
	 * @var string
	 *
	 * @ORM\Column(name="tel", type="string", length=255, nullable=true)
	 */
	private $tel;
	/**
	 * Home Phone
	 * field n° 18 in google export
	 * @var string
	 *
	 * @ORM\Column(name="home_phone", type="string", length=255, nullable=true)
	 */
	private $homePhone;
	/**
	 * Mobile Phone
	 * field n° 20 in google export
	 * @var string
	 *
	 * @ORM\Column(name="tel_mobile", type="string", length=255, nullable=true)
	 */
	private $telMobile;

	/**
	 * Home Address
	 * field n° 23 in google export
	 * @var string
	 *
	 * @ORM\Column(name="home_address", type="string", length=255, nullable=true)
	 */
	private $homeAddress;
	/**
	 * Home Address
	 * field n° 24 in google export
	 * @var string
	 *
	 * @ORM\Column(name="home_street", type="string", length=255, nullable=true)
	 */
	private $homeStreet;
	/**
	 * Home Address
	 * field n° 28 in google export
	 * @var string
	 *
	 * @ORM\Column(name="home_city", type="string", length=255, nullable=true)
	 */
	private $homeCity;
	/**
	 * Home Postal Code
	 * field n° 30 in google export
	 * @var string
	 *
	 * @ORM\Column(name="home_postal_code", type="string", length=255, nullable=true)
	 */
	private $homePostalCode;
	/**
	 * Home Country
	 * field n° 31 in google export
	 * @var string
	 *
	 * @ORM\Column(name="home_country", type="string", length=255, nullable=true)
	 */
	private $homeCountry;
	/**
	 * Company
	 * field n° 42
	 * @var string
	 *
	 * @ORM\Column(name="company", type="string", length=255, nullable=true)
	 */
	private $company;
	/**
	 * User 1, could be address mail
	 * field n° 75
	 * @var string
	 *
	 * @ORM\Column(name="user1", type="string", length=255, nullable=true)
	 */
	private $user1;
	/**
	 * User 2, could be address mail
	 * field n° 76
	 * @var string
	 *
	 * @ORM\Column(name="user2", type="string", length=255, nullable=true)
	 */
	private $user2;
	/**
	 * User 3, could be address mail
	 * field n° 77
	 * @var string
	 *
	 * @ORM\Column(name="user3", type="string", length=255, nullable=true)
	 */
	private $user3;
	/**
	 * Categories
	 * field n° 87
	 * @var string
	 *
	 * @ORM\Column(name="categories", type="string", length=255, nullable=true)
	 */
	private $categories;

	/**
	 * @var user who imported its sms
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="contacts")
	 */
	protected $owner;

	/**
	 * @var sms sent by the current contact
	 * @ORM\OneToMany(targetEntity="Sms", mappedBy="sender")
	 */
	protected $smsSent;
	/**
	 * @var sms sent by the current contact
	 * @ORM\OneToMany(targetEntity="Sms", mappedBy="reciever")
	 */
	protected $smsRecieved;
	/**
	 * @var sms sent by the current contact
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Conversation", mappedBy="contact")
	 */
	private $conversations;

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set firstName
	 *
	 * @param string $firstName
	 *
	 * @return Contact
	 */
	public function setFirstName( $firstName ) {
		$this->firstName = $firstName;

		return $this;
	}

	/**
	 * Get firstName
	 *
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * Set lastName
	 *
	 * @param string $lastName
	 *
	 * @return Contact
	 */
	public function setLastName( $lastName ) {
		$this->lastName = $lastName;

		return $this;
	}

	/**
	 * Get lastName
	 *
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * Set email
	 *
	 * @param string $email
	 *
	 * @return Contact
	 */
	public function setEmail( $email ) {
		$this->email = $email;

		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->owner = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set middleName
	 *
	 * @param string $middleName
	 *
	 * @return Contact
	 */
	public function setMiddleName( $middleName ) {
		$this->middleName = $middleName;

		return $this;
	}

	/**
	 * Get middleName
	 *
	 * @return string
	 */
	public function getMiddleName() {
		return $this->middleName;
	}

	/**
	 * Set email2
	 *
	 * @param string $email2
	 *
	 * @return Contact
	 */
	public function setEmail2( $email2 ) {
		$this->email2 = $email2;

		return $this;
	}

	/**
	 * Get email2
	 *
	 * @return string
	 */
	public function getEmail2() {
		return $this->email2;
	}

	/**
	 * Set tel
	 *
	 * @param string $tel
	 *
	 * @return Contact
	 */
	public function setTel( $tel ) {
		$this->tel = $tel;

		return $this;
	}

	/**
	 * Get tel
	 *
	 * @return string
	 */
	public function getTel() {
		return $this->tel;
	}

	/**
	 * Set tel2
	 *
	 * @param string $tel2
	 *
	 * @return Contact
	 */
	public function setTel2( $tel2 ) {
		$this->tel2 = $tel2;

		return $this;
	}

	/**
	 * Get tel2
	 *
	 * @return string
	 */
	public function getTel2() {
		return $this->tel2;
	}

	/**
	 * Set notes
	 *
	 * @param string $notes
	 *
	 * @return Contact
	 */
	public function setNotes( $notes ) {
		$this->notes = $notes;

		return $this;
	}

	/**
	 * Get notes
	 *
	 * @return string
	 */
	public function getNotes() {
		return $this->notes;
	}

	/**
	 * Add owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 *
	 * @return Contact
	 */
	public function addOwner( \AppBundle\Entity\User $owner ) {
		$this->owner[] = $owner;

		return $this;
	}

	/**
	 * Remove owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 */
	public function removeOwner( \AppBundle\Entity\User $owner ) {
		$this->owner->removeElement( $owner );
	}

	/**
	 * Get owner
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getOwner() {
		return $this->owner;
	}

	/**
	 * Set owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 *
	 * @return Contact
	 */
	public function setOwner( \AppBundle\Entity\User $owner = null ) {
		$this->owner = $owner;

		return $this;
	}

	/**
	 * Add smsSent
	 *
	 * @param \AppBundle\Entity\Sms $smsSent
	 *
	 * @return Contact
	 */
	public function addSmsSent( \AppBundle\Entity\Sms $smsSent ) {
		$this->smsSent[] = $smsSent;

		return $this;
	}

	/**
	 * Remove smsSent
	 *
	 * @param \AppBundle\Entity\Sms $smsSent
	 */
	public function removeSmsSent( \AppBundle\Entity\Sms $smsSent ) {
		$this->smsSent->removeElement( $smsSent );
	}

	/**
	 * Get smsSent
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getSmsSent() {
		return $this->smsSent;
	}

	/**
	 * Add smsRecieved
	 *
	 * @param \AppBundle\Entity\Sms $smsRecieved
	 *
	 * @return Contact
	 */
	public function addSmsRecieved( \AppBundle\Entity\Sms $smsRecieved ) {
		$this->smsRecieved[] = $smsRecieved;

		return $this;
	}

	/**
	 * Remove smsRecieved
	 *
	 * @param \AppBundle\Entity\Sms $smsRecieved
	 */
	public function removeSmsRecieved( \AppBundle\Entity\Sms $smsRecieved ) {
		$this->smsRecieved->removeElement( $smsRecieved );
	}

	/**
	 * Get smsRecieved
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getSmsRecieved() {
		return $this->smsRecieved;
	}

	/**
	 * Set birthDay.
	 *
	 * @param string|null $birthDay
	 *
	 * @return Contact
	 */
	public function setBirthDay( $birthDay = null ) {
		$this->birthDay = $birthDay;

		return $this;
	}

	/**
	 * Get birthDay.
	 *
	 * @return string|null
	 */
	public function getBirthDay() {
		return $this->birthDay;
	}

	/**
	 * Set email3.
	 *
	 * @param string $email3
	 *
	 * @return Contact
	 */
	public function setEmail3( $email3 ) {
		$this->email3 = $email3;

		return $this;
	}

	/**
	 * Get email3.
	 *
	 * @return string
	 */
	public function getEmail3() {
		return $this->email3;
	}

	/**
	 * Set telMobile.
	 *
	 * @param string|null $telMobile
	 *
	 * @return Contact
	 */
	public function setTelMobile( $telMobile = null ) {
		$this->telMobile = $telMobile;

		return $this;
	}

	/**
	 * Get telMobile.
	 *
	 * @return string|null
	 */
	public function getTelMobile() {
		return $this->telMobile;
	}

	/**
	 * Set homePhone.
	 *
	 * @param string|null $homePhone
	 *
	 * @return Contact
	 */
	public function setHomePhone( $homePhone = null ) {
		$this->homePhone = $homePhone;

		return $this;
	}

	/**
	 * Get homePhone.
	 *
	 * @return string|null
	 */
	public function getHomePhone() {
		return $this->homePhone;
	}

	/**
	 * Set homeAddress.
	 *
	 * @param string|null $homeAddress
	 *
	 * @return Contact
	 */
	public function setHomeAddress( $homeAddress = null ) {
		$this->homeAddress = $homeAddress;

		return $this;
	}

	/**
	 * Get homeAddress.
	 *
	 * @return string|null
	 */
	public function getHomeAddress() {
		return $this->homeAddress;
	}

	/**
	 * Set homeStreet.
	 *
	 * @param string|null $homeStreet
	 *
	 * @return Contact
	 */
	public function setHomeStreet( $homeStreet = null ) {
		$this->homeStreet = $homeStreet;

		return $this;
	}

	/**
	 * Get homeStreet.
	 *
	 * @return string|null
	 */
	public function getHomeStreet() {
		return $this->homeStreet;
	}

	/**
	 * Set homeCity.
	 *
	 * @param string|null $homeCity
	 *
	 * @return Contact
	 */
	public function setHomeCity( $homeCity = null ) {
		$this->homeCity = $homeCity;

		return $this;
	}

	/**
	 * Get homeCity.
	 *
	 * @return string|null
	 */
	public function getHomeCity() {
		return $this->homeCity;
	}

	/**
	 * Set homePostalCode.
	 *
	 * @param string|null $homePostalCode
	 *
	 * @return Contact
	 */
	public function setHomePostalCode( $homePostalCode = null ) {
		$this->homePostalCode = $homePostalCode;

		return $this;
	}

	/**
	 * Get homePostalCode.
	 *
	 * @return string|null
	 */
	public function getHomePostalCode() {
		return $this->homePostalCode;
	}

	/**
	 * Set homeCountry.
	 *
	 * @param string|null $homeCountry
	 *
	 * @return Contact
	 */
	public function setHomeCountry( $homeCountry = null ) {
		$this->homeCountry = $homeCountry;

		return $this;
	}

	/**
	 * Get homeCountry.
	 *
	 * @return string|null
	 */
	public function getHomeCountry() {
		return $this->homeCountry;
	}

	/**
	 * Set company.
	 *
	 * @param string|null $company
	 *
	 * @return Contact
	 */
	public function setCompany( $company = null ) {
		$this->company = $company;

		return $this;
	}

	/**
	 * Get company.
	 *
	 * @return string|null
	 */
	public function getCompany() {
		return $this->company;
	}

	/**
	 * Set user1.
	 *
	 * @param string|null $user1
	 *
	 * @return Contact
	 */
	public function setUser1( $user1 = null ) {
		$this->user1 = $user1;

		return $this;
	}

	/**
	 * Get user1.
	 *
	 * @return string|null
	 */
	public function getUser1() {
		return $this->user1;
	}

	/**
	 * Set user2.
	 *
	 * @param string|null $user2
	 *
	 * @return Contact
	 */
	public function setUser2( $user2 = null ) {
		$this->user2 = $user2;

		return $this;
	}

	/**
	 * Get user2.
	 *
	 * @return string|null
	 */
	public function getUser2() {
		return $this->user2;
	}

	/**
	 * Set user3.
	 *
	 * @param string|null $user3
	 *
	 * @return Contact
	 */
	public function setUser3( $user3 = null ) {
		$this->user3 = $user3;

		return $this;
	}

	/**
	 * Get user3.
	 *
	 * @return string|null
	 */
	public function getUser3() {
		return $this->user3;
	}

	/**
	 * Set categories.
	 *
	 * @param string|null $categories
	 *
	 * @return Contact
	 */
	public function setCategories( $categories = null ) {
		$this->categories = $categories;

		return $this;
	}

	/**
	 * Get categories.
	 *
	 * @return string|null
	 */
	public function getCategories() {
		return $this->categories;
	}

    /**
     * Add conversation.
     *
     * @param \AppBundle\Entity\Conversation $conversation
     *
     * @return Contact
     */
    public function addConversation(\AppBundle\Entity\Conversation $conversation)
    {
        $this->conversations[] = $conversation;

        return $this;
    }

    /**
     * Remove conversation.
     *
     * @param \AppBundle\Entity\Conversation $conversation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeConversation(\AppBundle\Entity\Conversation $conversation)
    {
        return $this->conversations->removeElement($conversation);
    }

    /**
     * Get conversations.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConversations()
    {
        return $this->conversations;
    }
}
