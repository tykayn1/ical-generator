<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evenement
 *
 * @ORM\Table(name="evenement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EvenementRepository")
 */
class Evenement {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="uid", type="string", length=255)
	 */
	private $uid;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=255, nullable=true)
	 */
	private $type;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="summary", type="string", length=255, nullable=true)
	 */
	private $summary;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var int
	 * date pour un évènement ponctuel
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;
	/**
	 * @var int
	 *
	 * @ORM\Column(name="date_originale", type="string", length=255, nullable=true)
	 */
	private $dateOriginale;
	/**
	 * creation of event
	 * @ORM\Column(name="date_created", type="string", length=255, nullable=true)
	 */
	private $dateCreated;

	/**
	 * creation of event
	 * @ORM\Column(name="date_modified", type="string", length=255, nullable=true)
	 */
	private $dateModified;

	/**
	 * début si évènement a une durée précise
	 * @var \DateTime
	 *
	 * @ORM\Column(name="start", type="datetime", nullable=true)
	 */
	private $start;

	/**
	 * fin si évènement a une durée précise
	 * @var \DateTime
	 *
	 * @ORM\Column(name="end", type="datetime", nullable=true)
	 */
	private $end;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="location", type="string", length=255, nullable=true)
	 */
	private $location;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="visibility", type="string", length=255, nullable=true)
	 */
	private $visibility;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="calendar_name", type="string", length=255, nullable=true)
	 */
	private $calendarName;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="prod_id", type="string", length=255, nullable=true)
	 */
	private $prodID;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="calendar_scale", type="string", length=255, nullable=true)
	 */
	private $calendarScale;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="categories", type="string", length=255, nullable=true)
	 */
	private $categories;
	/**
	 * repeat rules
	 * @var string
	 *
	 * @ORM\Column(name="repeat_rules", type="string", length=255, nullable=true)
	 */
	private $repeatRules;

	/**
	 * @var
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="events")
	 */
	protected $owner;
	/**
	 * @var
	 * @ORM\OneToOne(targetEntity="Conversation" )
	 */
	protected $conversation;
	/**
	 * @var
	 * @ORM\OneToOne(targetEntity="Research" )
	 */
	protected $research;

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set uid
	 *
	 * @param string $uid
	 *
	 * @return Evenement
	 */
	public function setUid( $uid ) {
		$this->uid = $uid;

		return $this;
	}

	/**
	 * Get uid
	 *
	 * @return string
	 */
	public function getUid() {
		return $this->uid;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 *
	 * @return Evenement
	 */
	public function setType( $type ) {
		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Set summary
	 *
	 * @param string $summary
	 *
	 * @return Evenement
	 */
	public function setSummary( $summary ) {
		$this->summary = $summary;

		return $this;
	}

	/**
	 * Get summary
	 *
	 * @return string
	 */
	public function getSummary() {
		return $this->summary;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 *
	 * @return Evenement
	 */
	public function setDescription( $description ) {
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set date
	 *
	 * @param integer $date
	 *
	 * @return Evenement
	 */
	public function setDate( $date ) {
		$this->date = $date;

		return $this;
	}

	/**
	 * Get date
	 *
	 * @return int
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Set start
	 *
	 * @param \DateTime $start
	 *
	 * @return Evenement
	 */
	public function setStart( $start ) {
		$this->start = $start;

		return $this;
	}

	/**
	 * Get start
	 *
	 * @return \DateTime
	 */
	public function getStart() {
		return $this->start;
	}

	/**
	 * Set end
	 *
	 * @param \DateTime $end
	 *
	 * @return Evenement
	 */
	public function setEnd( $end ) {
		$this->end = $end;

		return $this;
	}

	/**
	 * Get end
	 *
	 * @return \DateTime
	 */
	public function getEnd() {
		return $this->end;
	}

	/**
	 * Set location
	 *
	 * @param string $location
	 *
	 * @return Evenement
	 */
	public function setLocation( $location ) {
		$this->location = $location;

		return $this;
	}

	/**
	 * Get location
	 *
	 * @return string
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * Set visibility
	 *
	 * @param string $visibility
	 *
	 * @return Evenement
	 */
	public function setVisibility( $visibility ) {
		$this->visibility = $visibility;

		return $this;
	}

	/**
	 * Get visibility
	 *
	 * @return string
	 */
	public function getVisibility() {
		return $this->visibility;
	}

	/**
	 * Set categories
	 *
	 * @param string $categories
	 *
	 * @return Evenement
	 */
	public function setCategories( $categories ) {
		$this->categories = $categories;

		return $this;
	}

	/**
	 * Get categories
	 *
	 * @return string
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->owner = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 *
	 * @return Evenement
	 */
	public function addOwner( \AppBundle\Entity\User $owner ) {
		$this->owner[] = $owner;

		return $this;
	}

	/**
	 * Remove owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 */
	public function removeOwner( \AppBundle\Entity\User $owner ) {
		$this->owner->removeElement( $owner );
	}

	/**
	 * Get owner
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getOwner() {
		return $this->owner;
	}

	/**
	 * Set owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 *
	 * @return Evenement
	 */
	public function setOwner( \AppBundle\Entity\User $owner = null ) {
		$this->owner = $owner;

		return $this;
	}

	/**
	 * Set conversation
	 *
	 * @param \AppBundle\Entity\Conversation $conversation
	 *
	 * @return Evenement
	 */
	public function setConversation( \AppBundle\Entity\Conversation $conversation = null ) {
		$this->conversation = $conversation;

		return $this;
	}

	/**
	 * Get conversation
	 *
	 * @return \AppBundle\Entity\Conversation
	 */
	public function getConversation() {
		return $this->conversation;
	}

	/**
	 * Set research
	 *
	 * @param \AppBundle\Entity\Research $research
	 *
	 * @return Evenement
	 */
	public function setResearch( \AppBundle\Entity\Research $research = null ) {
		$this->research = $research;

		return $this;
	}

	/**
	 * Get research
	 *
	 * @return \AppBundle\Entity\Research
	 */
	public function getResearch() {
		return $this->research;
	}

	/**
	 * @return int
	 */
	public function getDateOriginale() {
		return $this->dateOriginale;
	}

	/**
	 * @param int $dateOriginale
	 */
	public function setDateOriginale( $dateOriginale ) {
		$this->dateOriginale = $dateOriginale;
	}

	/**
	 * @return string
	 */
	public function getProdID() {
		return $this->prodID;
	}

	/**
	 * @param string $prodID
	 */
	public function setProdID( $prodID ) {
		$this->prodID = $prodID;
	}

	/**
	 * @return string
	 */
	public function getCalendarScale() {
		return $this->calendarScale;
	}

	/**
	 * @param string $calendarScale
	 */
	public function setCalendarScale( $calendarScale ) {
		$this->calendarScale = $calendarScale;
	}

	/**
	 * @return string
	 */
	public function getCalendarName() {
		return $this->calendarName;
	}

	/**
	 * @param string $calendarName
	 */
	public function setCalendarName( $calendarName ) {
		$this->calendarName = $calendarName;
	}

	/**
	 * @return string
	 */
	public function getRepeatRules() {
		return $this->repeatRules;
	}

	/**
	 * @param string $repeatRules
	 */
	public function setRepeatRules( $repeatRules ) {
		$this->repeatRules = $repeatRules;
	}

	/**
	 * @return mixed
	 */
	public function getDateCreated() {
		return $this->dateCreated;
	}

	/**
	 * @param mixed $dateCreated
	 */
	public function setDateCreated( $dateCreated ) {
		$this->dateCreated = $dateCreated;
	}

	/**
	 * @return mixed
	 */
	public function getDateModified() {
		return $this->dateModified;
	}

	/**
	 * @param mixed $dateModified
	 */
	public function setDateModified( $dateModified ) {
		$this->dateModified = $dateModified;
	}
}
