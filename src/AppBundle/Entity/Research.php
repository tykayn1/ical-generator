<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Research
 *
 * @ORM\Table(name="research")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ResearchRepository")
 */
class Research {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="queryText", type="string", length=400)
	 */
	private $queryText;

	/**
	 * @var \DateTime
	 * google search are grouped on multiple times.
	 * for simplicity i use the first timestamp found in the export
	 * @ORM\Column(name="date", type="datetime" )
	 */
	private $date;
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="multiple_date", type="string", length=500, nullable=true)
	 */
	private $multipleDate;

	/**
	 * @var
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="researchs")
	 */
	protected $owner;
	/**
	 * @var
	 * @ORM\OneToOne(targetEntity="Evenement")
	 */
	protected $evenement;

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set queryText
	 *
	 * @param string $queryText
	 *
	 * @return Research
	 */
	public function setQueryText( $queryText ) {
		$this->queryText = $queryText;

		return $this;
	}

	/**
	 * Get queryText
	 *
	 * @return string
	 */
	public function getQueryText() {
		return $this->queryText;
	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 *
	 * @return Research
	 */
	public function setDate( $date ) {
		$this->date = $date;

		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Set owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 *
	 * @return Research
	 */
	public function setOwner( \AppBundle\Entity\User $owner = null ) {
		$this->owner = $owner;

		return $this;
	}

	/**
	 * Get owner
	 *
	 * @return \AppBundle\Entity\User
	 */
	public function getOwner() {
		return $this->owner;
	}

	/**
	 * Set evenement
	 *
	 * @param \AppBundle\Entity\Evenement $evenement
	 *
	 * @return Research
	 */
	public function setEvenement( \AppBundle\Entity\Evenement $evenement = null ) {
		$this->evenement = $evenement;

		return $this;
	}

	/**
	 * Get evenement
	 *
	 * @return \AppBundle\Entity\Evenement
	 */
	public function getEvenement() {
		return $this->evenement;
	}

	/**
	 * Set multipleDate.
	 *
	 * @param string|null $multipleDate
	 *
	 * @return Research
	 */
	public function setMultipleDate( $multipleDate = null ) {
		$this->multipleDate = $multipleDate;

		return $this;
	}

	/**
	 * Get multipleDate.
	 *
	 * @return string|null
	 */
	public function getMultipleDate() {
		return $this->multipleDate;
	}
}
