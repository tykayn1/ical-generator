<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sms
 *
 * @ORM\Table(name="sms")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SmsRepository")
 */
class Sms {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="senderTel", type="string", length=255, nullable=true)
	 */
	private $senderTel;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="senderName", type="string", length=255, nullable=true)
	 */
	private $senderName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="string", length=500, nullable=true)
	 */
	private $content;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="date_raw", type="string", length=100, nullable=true)
	 */
	private $dateRaw;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="recieverTel", type="string", length=255, nullable=true)
	 */
	private $recieverTel;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dateReception", type="datetime", nullable=true)
	 */
	private $dateReception;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dateSent", type="datetime", nullable=true)
	 */
	private $dateSent;

	/**
	 * @var
	 * @ORM\ManyToOne(targetEntity="Contact", inversedBy="smsSent")
	 */
	protected $sender;
	/**
	 * @var
	 * @ORM\ManyToOne(targetEntity="Contact", inversedBy="smsRecieved")
	 */
	protected $reciever;

	/**
	 * @var
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="sms")
	 */
	protected $owner;

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set senderTel
	 *
	 * @param string $senderTel
	 *
	 * @return Sms
	 */
	public function setSenderTel( $senderTel ) {
		$this->senderTel = $senderTel;

		return $this;
	}

	/**
	 * Get senderTel
	 *
	 * @return string
	 */
	public function getSenderTel() {
		return $this->senderTel;
	}

	/**
	 * Set content
	 *
	 * @param string $content
	 *
	 * @return Sms
	 */
	public function setContent( $content ) {
		$this->content = $content;

		return $this;
	}

	/**
	 * Get content
	 *
	 * @return string
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Set recieverTel
	 *
	 * @param string $recieverTel
	 *
	 * @return Sms
	 */
	public function setRecieverTel( $recieverTel ) {
		$this->recieverTel = $recieverTel;

		return $this;
	}

	/**
	 * Get recieverTel
	 *
	 * @return string
	 */
	public function getRecieverTel() {
		return $this->recieverTel;
	}

	/**
	 * Set dateReception
	 *
	 * @param \DateTime $dateReception
	 *
	 * @return Sms
	 */
	public function setDateReception( $dateReception ) {
		$this->dateReception = $dateReception;

		return $this;
	}

	/**
	 * Get dateReception
	 *
	 * @return \DateTime
	 */
	public function getDateReception() {
		return $this->dateReception;
	}

	/**
	 * Set dateSent
	 *
	 * @param \DateTime $dateSent
	 *
	 * @return Sms
	 */
	public function setDateSent( $dateSent ) {
		$this->dateSent = $dateSent;

		return $this;
	}

	/**
	 * Get dateSent
	 *
	 * @return \DateTime
	 */
	public function getDateSent() {
		return $this->dateSent;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->owner = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 *
	 * @return Sms
	 */
	public function addOwner( \AppBundle\Entity\User $owner ) {
		$this->owner[] = $owner;

		return $this;
	}

	/**
	 * Remove owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 */
	public function removeOwner( \AppBundle\Entity\User $owner ) {
		$this->owner->removeElement( $owner );
	}

	/**
	 * Get owner
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getOwner() {
		return $this->owner;
	}

	/**
	 * Set sender
	 *
	 * @param \AppBundle\Entity\Contact $sender
	 *
	 * @return Sms
	 */
	public function setSender( \AppBundle\Entity\Contact $sender = null ) {
		$this->sender = $sender;

		return $this;
	}

	/**
	 * Get sender
	 *
	 * @return \AppBundle\Entity\Contact
	 */
	public function getSender() {
		return $this->sender;
	}

	/**
	 * Set reciever
	 *
	 * @param \AppBundle\Entity\Contact $reciever
	 *
	 * @return Sms
	 */
	public function setReciever( \AppBundle\Entity\Contact $reciever = null ) {
		$this->reciever = $reciever;

		return $this;
	}

	/**
	 * Get reciever
	 *
	 * @return \AppBundle\Entity\Contact
	 */
	public function getReciever() {
		return $this->reciever;
	}

	/**
	 * Set owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 *
	 * @return Sms
	 */
	public function setOwner( \AppBundle\Entity\User $owner = null ) {
		$this->owner = $owner;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getSenderName() {
		return $this->senderName;

		return $this;
	}

	/**
	 * @param string $senderName
	 */
	public function setSenderName( $senderName ) {
		$this->senderName = $senderName;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getDateRaw() {
		return $this->dateRaw;

		return $this;
	}

	/**
	 * @param string $dateRaw
	 */
	public function setDateRaw( $dateRaw ) {
		$this->dateRaw = $dateRaw;

		return $this;
	}
}
